Implemented Alexnet network in Python using TensorFlow.
Alexnet is a Convolutional Neural Network used for Object Detection.
The weights of a pre-trained Alexnet CNN were used to initialise the weights the local model to avoid training time.
Alexnet network is trained on 1000 classes and consists of conv,pool and batch norm layers.